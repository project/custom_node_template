<?php

/**
 * Custom Node Template allows one to specify different node templates to be used on a node by node basis or by content type.  
 */

/**
 * Implementation of hook_help().
 */
function custom_node_template_help($path, $arg) {
  switch ($path) {
    case 'admin/help#custom_node_template':
      $output = "<p>Custom Node Template allows one to specify different node templates to be used on a node by node basis or by content type.</p>  
		  <p>Please note that depending on the status of this module and the version you are using, you may
		  also found additional information in the <a href='http://drupal.org/node/639580'>Custom Node Template
		  Handbook Page on Drupal.org</a>.</p>                
		  <h2>Settings</h2>
                  <p>You can specify the use of a specific node template (node.tpl) file when you are creating or 
		  editing a node.  Under Node Template Settings, you will find a select list of the node templates
		  in your current theme.  Simply the select the one you wish to have used when the node is displayed.
		  You can also specify the node template to be used by default in the Content Type settings.  Nodes
		  you create will use this setting as the default unless you choose a different node template when 
		  you create a new node.  <b>Please Note</b> that this does not mean that specifying a node template
		  in a content type will affect existing nodes of that content type.  It simply means that the node
		  template will be listed as the default template to be used when you create a new node.</p>
		  <p>When using this module, it is not necessary to specify the use of a particular node template. 
		  If one is not specified, the default node template will be loaded.  If you have more than one
		  node template in your theme, then Drupal will load a specific node template based on existing
		  template suggestions and available node templates.  If you do specify a node template, then your
		  selection will override default node templates and template suggestions.</p>
                  <h2>Custom Templates</h2>
		  <p>To add a custom Node Template, simply create an additional node.tpl file and add it to your
		  theme.  The format for naming the file should be node-XXXXXX.tpl where XXXXXX is a custom name
		  that you specify.  See the Tips section below on some suggestions for naming schemes.</p>
                  <h2>Tips</h2>
                  <p>If you are only using one node template per content type, you do not need to specify the node
		  template in the Content Type settings.  By default, Drupal will load the appropriate node template 
		  for the content type if one exists in your theme.  For such usage, you actually don't need to use 
		  this module at all.</p>
		  <p>There are some naming schemes you will probably wish to avoid when adding custom node templates 
		  to your theme.  For example, unless you are doing so deliberately, you will probably not want to 
		  name a custom node template file node-4.tpl.  Depending on your theme, you may have a preprocess
		  function that adds the Node ID as a template suggestion for Drupal.  If this is the case with the
		  theme you are using, Drupal will load the node-4.tpl file for Node/4.  If you name your custom node
		  template with this same name, it would work when you choose the node template for a particular node,
		  but it will also cause the same node template to be loaded for Node/4 which you may or may not wish
		  to have happen.  As another example, you could include a node template that has a content type in 
		  the name of the node template.  But if you don't want to have that node template loaded as the default
		  for that content type, the another name would probably be better.</p>";
  }
  return $output;
}

/**
 * Implementation of hook_perm().
 */
function custom_node_template_perm() {
  return array('administer custom node templates');
}

/**
 * Implementation of hook_nodeapi().
 */
function custom_node_template_nodeapi(&$node, $op) {

  switch ($op) {
    case 'insert':

      if (isset($node->nodetemplate)) {
        db_query("INSERT INTO {custom_node_template} (nid, nodetemplate) VALUES ('%d', '%s')", $node->nid, $node->nodetemplate);
      }
      break;

    case 'update':

      $nodetemplate = db_result(db_query("SELECT nodetemplate FROM {custom_node_template} WHERE nid = '%d'", $node->nid));
      if (isset($node->nodetemplate)) {
        if (empty($nodetemplate)) {
          db_query("INSERT INTO {custom_node_template} (nid, nodetemplate) VALUES ('%d', '%s')", $node->nid, $node->nodetemplate);
        }
        else {
          db_query("UPDATE {custom_node_template} SET nodetemplate = '%s' WHERE nid = '%d'", $node->nodetemplate, $node->nid);
        }
      }
      else {
        db_query("DELETE FROM {custom_node_template} WHERE nid = '%d'", $node->nid);
      }
      break;

    case 'delete':

      db_query("DELETE FROM {custom_node_template} WHERE nid = '%d'", $node->nid);
      break;

    case 'load':
    case 'prepare':

      $nodetemplate = db_result(db_query("SELECT nodetemplate FROM {custom_node_template} WHERE nid = '%d'", $node->nid));

      if (empty($nodetemplate)) {
        if (variable_get('nodetemplate_'. $node->type, FALSE)) {
          $nodetemplate = variable_get('nodetemplate_'. $node->type, FALSE);
        }
        else {
          $nodetemplate = 'default';
        }
      }

      $node->nodetemplate = $nodetemplate;

      break;
  }
}

/**
 * Implementation of hook_node_type().
 */
function custom_node_template_node_type($op, $info) {

  switch ($op) {
    case 'delete':
      variable_del('nodetemplate_'. $info->type);
      break;
  }
}

/**
 * Implementation of hook_form_alter().
 */
function custom_node_template_form_alter(&$form, $form_state, $form_id) {
  drupal_add_css(drupal_get_path('module', 'custom_node_template') .'/custom_node_template.css');
  if ($form['#node']->type .'_node_form' == $form_id) {
    $form['nodetemplate'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node template settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#access' => user_access('administer custom node templates'),
      '#group' => t('Node template settings') 
    );
    $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
    $nodetemplate = $form['#node']->nodetemplate;
    if (empty($nodetemplate)) $nodetemplate = 'default';

    $form['nodetemplate']['nodetemplate'] = array(
      '#type' => 'radios',
      '#title' => t('Node template'),
      '#description' => t('Select the node template to be used for displaying this node.  If "default" is selected, then your node will be displayed per existing rules (node template suggestions) for your theme.  All applicable node templates (node-XXX.tpl.php files) are listed here.  Your main node.tpl.php file is listed as "Main Node Template". See Online Help (/admin/help) for more information on this setting.'),
      '#default_value' => $nodetemplate,
      '#options' => _custom_node_templates(),
      '#access' => user_access('administer custom node templates')
    );
    $form['nodetemplate']['#item'] = $nodetemplate;
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function custom_node_template_form_node_type_form_alter(&$form, &$from_state) {
  drupal_add_css(drupal_get_path('module', 'custom_node_template') .'/custom_node_template.css');

  if (isset($form['identity']['type'])) {
    $form['nodetemplate'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node template settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => t('Node template settings')
    );
    $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
    $nodetemplate = variable_get('nodetemplate_' . $form['#node_type']->type, 'default');

    $form['nodetemplate']['nodetemplate'] = array(
      '#type' => 'radios',
      '#title' => t('Node template'),
      '#description' => t('Select the default node template to be used when adding NEW nodes of this content type.  Changing this setting will not change the display of nodes you created previously.  If "default" is selected, then your node will be displayed per existing rules (node template suggestions) for your theme.  All applicable node templates (node-XXX.tpl.php files) are listed here.  Your main node.tpl.php file is listed as "Main Node Template". See Online Help (/admin/help) for more information on this setting.'),
      '#default_value' => $nodetemplate,
      '#options' => _custom_node_templates()
    );
    $form['nodetemplate']['#item'] = $nodetemplate;
  }
}

/**
* Implementation of hook_content_extra_fields().
*/
function custom_node_template_content_extra_fields($type_name) {
    $extra['custom_node_template'] = array(
      'label' => t('Node template settings'),
      'description' => t('Custom note template settings fieldset.'),
      'weight' => 3,
    );
	return $extra;
}


function _custom_node_templates() {

  $theme = variable_get('theme_default', 'garland');
  $theme_path = drupal_get_path('theme', $theme) .'/';
    $options = array('default' => '<img class="custom_node_template" src="'.base_path() .drupal_get_path('module', 'custom_node_template').'/images/default.png" title="'.$displayname.'" /> '.t('default'));

  $handler = opendir($theme_path);
  while ($file = readdir($handler)) {
    if ($file != '.' && $file != '..' && substr($file, 0, 4) == 'node' && substr($file, -8, 8) == '.tpl.php' && substr($file, -13, 5) != '-edit') {
        $name = drupal_substr($file, 0, -8);
        $displayname = substr_replace($name, '', 0, 5);
        $displayname = str_replace('-', ' ', $displayname);
        $displayname = str_replace('_', ' ', $displayname);
        if (empty($displayname)) { $displayname = t('Main Node Template');}
        $options[$name] = $displayname;
        if (file_exists(drupal_get_path('module', 'custom_node_template')."/images/$name.png")) { $options[$name] = '<img class="custom_node_template" src="'.base_path() .drupal_get_path('module', 'custom_node_template').'/images/'.$name.'.png" title="'.$displayname.'" /> '.$displayname; }
        if (file_exists("$theme_path/$name.png")) { $options[$name] = '<img class="custom_node_template" src="'.base_path() .$theme_path.'/'.$name.'.png" title="'.$displayname.'" /> '.$displayname; }

    }
  }
  closedir($handler);

  return $options;
}

/**
 * Implementation of hook_preprocess_node().
 */
function custom_node_template_preprocess_node(&$variables) {

  if (drupal_substr(request_uri(), -5) != '/edit') {
    if ($variables['nodetemplate'] != 'default') {
      $nodetemplate = 'node-'. drupal_substr($variables['nodetemplate'], 5);
      $variables['template_files'][] = $variables['node']->nodetemplate;
    }
  }
}